import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import App from './App';
import { createMuiTheme, ThemeProvider } from '@material-ui/core';
import {  blueGrey,   deepOrange } from '@material-ui/core/colors';

const theme = createMuiTheme({
    palette:{
        primary:{main:deepOrange[300]},
        secondary:{main:blueGrey[800]}
    },
    typography:{
        fontFamily:"Vazir"
    },
    direction: 'rtl',
});

export default function AppRouter() {
    return (
        <ThemeProvider theme={theme}>
        <BrowserRouter>
            <App />
        </BrowserRouter>
        </ThemeProvider>
    );
}