import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import AppRouter from './AppRouter';
// import 'vazir-font';

ReactDOM.render(<AppRouter />, document.getElementById('root'));
