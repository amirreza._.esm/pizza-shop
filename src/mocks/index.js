import img1 from '../imgRes/1.jpg';
import img2 from '../imgRes/2.jpg';
import img3 from '../imgRes/3.jpg';
import custom from '../imgRes/custom.jpg';

export const imgCustom =  custom;

export const americanList = [
    {
        id: 0,
        title: "ژامبون",
        description:"چدار ، سس گوجه فرنگی ، سس سیر ، پنیر موزارلا ، قارچ ، پنیر بلوچيز ، فیله گوساله",
        price: 50,
        avatar: img1,
        rating: 5
    }, {
        id: 1,
        title: "مخصوص",
        description:"گوشت قلقلی ، ذرت ، سس مکزیکی ، پیاز ، پنیر موزارلا ، هالپینو ، زیتون سیاه ، فلفل دلمه‌ای",
        price: 39,
        avatar: img2,
        rating: 3.5
    }, {
        id: 2,
        title: "پپرونی",
        description:"پنیر پارمزان ، پنیر بلوچيز ، سس گوجه فرنگی ، ژامبون ، پنیر گودا ، پنیر موزارلا ، سالامی ، چدار ، استیک ، قارچ",
        price: 99,
        avatar: img3,
        rating: 4
    }, {
        id: 3,
        title: "پیتزا چانو (استیک)",
        description:"چدار ، سس گوجه فرنگی ، سس سیر ، پنیر موزارلا ، قارچ ، پنیر بلوچيز ، فیله گوساله",
        price: 125,
        avatar: img3,
        rating: 4
    }, {
        id: 4,
        title: "پیتزا چهار مزه",
        description:"سینه مرغ گریل شده ، گوجه گیلاسی ، زیتون سبز ، قارچ ، سس گوجه فرنگی ، پیاز ، استیک ، زیتون سیاه ، فلفل دلمه‌ای ، اسفناج ، ذرت ، ژامبون بوقلمون",
        price: 77,
        avatar: img3,
        rating: 4
    }, {
        id: 5,
        title: "پیتزا آفریکانا (مرغ)",
        description:"سینه مرغ گریل شده ، سس سفید مخصوص ، پنیر گودا ، فلفل دلمه‌ای ، چدار ، قارچ ، پنیر موزارلا",
        price: 53,
        avatar: img3,
        rating: 4
    }, {
        id: 6,
        title: "پیتزا سبزیجات",
        description:"کدو ، پنیر موزارلا ، گوجه گیلاسی ، قارچ ، اسفناج ، بادمجان ، سس گوجه فرنگی ، کاپری ، پیاز ، زیتون سبز",
        price: 44,
        avatar: img3,
        rating: 4
    },
];

export const italianList = [
    {
        id: 0,
        title: "ژامبون",
        description:"چدار ، سس گوجه فرنگی ، سس سیر ، پنیر موزارلا ، قارچ ، پنیر بلوچيز ، فیله گوساله",
        price: 50,
        avatar: img1,
        rating: 5
    }, {
        id: 1,
        title: "مخصوص",
        description:"گوشت قلقلی ، ذرت ، سس مکزیکی ، پیاز ، پنیر موزارلا ، هالپینو ، زیتون سیاه ، فلفل دلمه‌ای",
        price: 39,
        avatar: img2,
        rating: 3.5
    }, {
        id: 2,
        title: "پپرونی",
        description:"پنیر پارمزان ، پنیر بلوچيز ، سس گوجه فرنگی ، ژامبون ، پنیر گودا ، پنیر موزارلا ، سالامی ، چدار ، استیک ، قارچ",
        price: 99,
        avatar: img3,
        rating: 4
    },
];

export const burgerList = [
    {
        id: 0,
        title: "ژامبون",
        description:"چدار ، سس گوجه فرنگی ، سس سیر ، پنیر موزارلا ، قارچ ، پنیر بلوچيز ، فیله گوساله",
        price: 50,
        avatar: img1,
        rating: 5
    }, {
        id: 1,
        title: "مخصوص",
        description:"گوشت قلقلی ، ذرت ، سس مکزیکی ، پیاز ، پنیر موزارلا ، هالپینو ، زیتون سیاه ، فلفل دلمه‌ای",
        price: 39,
        avatar: img2,
        rating: 3.5
    }, {
        id: 2,
        title: "پپرونی",
        description:"پنیر پارمزان ، پنیر بلوچيز ، سس گوجه فرنگی ، ژامبون ، پنیر گودا ، پنیر موزارلا ، سالامی ، چدار ، استیک ، قارچ",
        price: 99,
        avatar: img3,
        rating: 4
    },
];

export const drinkList = [
    {
        id: 0,
        title: "ژامبون",
        description:"چدار ، سس گوجه فرنگی ، سس سیر ، پنیر موزارلا ، قارچ ، پنیر بلوچيز ، فیله گوساله",
        price: 50,
        avatar: img1,
        rating: 5
    }, {
        id: 1,
        title: "مخصوص",
        description:"گوشت قلقلی ، ذرت ، سس مکزیکی ، پیاز ، پنیر موزارلا ، هالپینو ، زیتون سیاه ، فلفل دلمه‌ای",
        price: 39,
        avatar: img2,
        rating: 3.5
    }, {
        id: 2,
        title: "پپرونی",
        description:"پنیر پارمزان ، پنیر بلوچيز ، سس گوجه فرنگی ، ژامبون ، پنیر گودا ، پنیر موزارلا ، سالامی ، چدار ، استیک ، قارچ",
        price: 99,
        avatar: img3,
        rating: 4
    },
];

export const coupons = [
    {
        title : "25%",
        code : "qwerty123",
        discount: 0.25
    },
    {
        title: "50%",
        code : "soli<3",
        discount : 0.5
    }
]


export const customContent = [
    {
        title:"پنیر",
        options : [
            {text: "چدار", price: 10}, 
            {text: "سفید", price: 15}, 
            {text: "گودا", price: 5}]
    },
    {
        title:"گوشت",
        options : [
            {text: "ژامبون", price: 20}, 
            {text: "مرغ", price: 15}, 
            {text: "استیک", price: 10}]
    },
    {
        title:"سس",
        options : [
            {text: "تند", price: 10}, 
            {text: "کچاپ", price: 5}, 
            {text: "فرانسوی", price: 10}]
    },
];