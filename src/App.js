import React, { useState } from 'react';
import './App.css';
import { AppBar, Toolbar, Button, makeStyles, Typography, Dialog, IconButton, Badge, TextField } from '@material-ui/core';
import { useHistory, Switch, Route } from 'react-router-dom';
import Home from './components/Home';
import About from './components/About';
import PurchaseList from './components/modals/PurchaseList';
import { LocalPizza, ShoppingBasket, Home as HomeIcon, Instagram, Facebook, Twitter } from '@material-ui/icons';
import CustomPizza from './components/modals/CustomPizza/CustomPizza';
import Login from './components/modals/Login';
import SignUp from './components/modals/SignUp';

const useStyles = makeStyles(theme => ({
  appRoot:{
    height: '100%',
    backgroundColor:theme.palette.secondary.light,
    direction:'rtl'
  },
  appMain:{
    width:'100%'
  },
  toolbarRoot: {
  },
  toolbarTitle: {
    flexGrow: 1,
    textAlign: 'left',
    fontWeight: 700,
    marginLeft: 10
  },
  footer: {
    display:'flex',
    backgroundColor: theme.palette.secondary.main
  }
}));

function App() {
  const classes = useStyles();
  const history = useHistory();
  const [selectList, setSelectList] = useState([]);
  const [open, setOpen] = useState(false);
  const [openPizza, setOpenPizza] = useState(false);
  const [openLogin, setOpenLogin] = useState(false);
  const [openSignUp, setOpenSignUp] = useState(false);



  const addToSelectList = item => {
    let found = false;
    const newList = selectList.map(el => {
      if(el.productItem.id === item.id && el.productItem.title === item.title) {
        found = true;
        return {productItem: item, count: el.count + 1};
      }
      return el;
    });
    if (!found)
      newList.push({productItem: item, count : 1});
    setSelectList( newList );
  }

  const updateItemCount = (item, count) => {
    const newList = selectList.map(el => {
      if(el.productItem.id === item.id) {
        return {productItem: item, count: count};
      }
      return el;
    });
    setSelectList(newList);
  };

  const removeFromSelectList = item => {
    setSelectList( selectList.filter(el => el.productItem.id !== item.id) );
  };

  const closePurchaseDialog = () => setOpen(false);

  return (
    <div className={classes.appRoot}>

      <AppBar>
        <Toolbar>
          <IconButton style={{color:"black"}} onClick={() => { history.push('/') }}><HomeIcon /></IconButton>
          <IconButton style={{color:"black"}} onClick={() => { setOpen(true)}}>
            <Badge color="error" badgeContent={selectList.length} invisible={(selectList.length === 0)}>
              <ShoppingBasket />
            </Badge>
          </IconButton>
          <Button onClick={() => { setOpenLogin(true) }}>ورود/عضویت</Button>
          <Button onClick={() => { history.push('/about') }}>درباره ما</Button>
          <TextField  color="secondary" style={{marginRight: 10}} variant="outlined" size="small" label="جستجو" />
          <Typography className={classes.toolbarTitle} variant="h5">پیتزا شاپ</Typography>
          <LocalPizza />
        </Toolbar>
      </AppBar>
      <Toolbar />
      <div className={classes.appMain} >
        <Switch>
          <Route path="/" exact >
            <Home 
              addHandler={addToSelectList}
              openPizzaHandler={()=>setOpenPizza(true)}
              />
          </Route>
          <Route path="/about" exact >
            <About />
          </Route>
        </Switch>
      </div>
      <div>
        <Toolbar className={classes.footer}>
          <Typography variant="caption" style={{flexGrow: 1, textAlign: "center"}}> تمامی حقوق متعلق به ...</Typography>
          <IconButton><Instagram /></IconButton>
          <IconButton><Facebook /></IconButton>
          <IconButton><Twitter /></IconButton>
        </Toolbar>
      </div>
      <Dialog open={open} onClose={()=> setOpen(false)}>
        <PurchaseList closeDialog={closePurchaseDialog} selectList={selectList} updateHandler={updateItemCount} removeHandler={removeFromSelectList}/>
      </Dialog>
      <Dialog open={openPizza} onClose={()=> setOpenPizza(false)}>
        <CustomPizza addHandler={addToSelectList} closePizza={()=>{setOpenPizza(false)}}/>
      </Dialog>
      <Dialog open={openLogin} onClose={()=>{setOpenLogin(false)}}>
        <Login closeLogin={()=>{setOpenLogin(false)}} openSignUp={()=>{setOpenSignUp(true)}}/>
      </Dialog>
      <Dialog open={openSignUp} onClose={()=>{setOpenSignUp(false)}}>
        <SignUp />
      </Dialog>
    </div>
  );
}

export default App;
