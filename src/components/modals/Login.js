import React, { useState, useEffect } from 'react';
import { Paper, List, Typography, Button, makeStyles, Divider, TextField, Grid, useTheme, FormControlLabel, Radio, FormLabel, InputAdornment } from '@material-ui/core';
import { AccountCircle, Lock, LockOpen, Email, AlternateEmail } from '@material-ui/icons';

const useStyles = makeStyles(theme => ({
    paper: {
        // height: 500,
        // width: 400,
        padding: 30,
        display:'flex',
        flexDirection: 'column',
        alignItems: 'center'
    },
    form: {
        // height: 400,
        width: 275,
        margin: 25,
    }
}));

const Login = (props) => {

    const classes = useStyles();
    const {closeLogin, openSignUp} = props;

    const handleCreateAcc = () => {
        closeLogin();
        openSignUp();
    }

    return (
        <Paper className={classes.paper}>
            <Typography color="secondary" style={{fontWeight:700}} variant="h4" align="center" >ورود</Typography>
            <LockOpen color="primary" style={{marginTop: 40, fontSize: '4em'}}/>
            {/* <div style={{height:'300px', display:'flex', flexDirection: 'column', justifyContent:'center'}}> */}
                <div className={classes.form}>
                    <TextField style={{marginTop: 15, marginBottom: 10}}
                        variant="outlined" size="small"
                        fullWidth
                        label="ایمیل"
                        InputProps={{
                            startAdornment: (
                                <InputAdornment position="start">
                                    <AlternateEmail />
                                </InputAdornment>
                            )
                        }}
                    />
                    <TextField style={{marginTop: 15, marginBottom: 10}}
                        type="password"
                        variant="outlined" size="small"
                        fullWidth
                        label="رمزعبور"
                        InputProps={{
                            startAdornment: (
                                <InputAdornment position="start">
                                    <Lock />
                                </InputAdornment>
                            )
                        }}
                    />
                    <Button style={{marginTop: 20}} color="primary" fullWidth variant="contained" size="large">ورود</Button>
                    <Button style={{marginTop: 5}} color="secondary" fullWidth variant="text" size="small" onClick={handleCreateAcc}>ساخت حساب کاربری جدید</Button>
                </div>
            {/* </div> */}
        </Paper>
    );
};

export default Login;