import React, { useState } from 'react';
import { ListItem, ListItemAvatar, ListItemText, Avatar, TextField, Button, makeStyles, Grid, Typography, IconButton } from '@material-ui/core';
import {Delete} from '@material-ui/icons';

const useStyles = makeStyles(theme => ({
    listItem : {
        display: 'flex',
        flexDirection: 'row',
        // paddingLeft:0,
        // paddingRight:0,
        textAlign: 'right',
    },
    xButton:{
        minWidth: 20,
        maxWidth: 30,
    },
    priceLabel: {
        flex : '0 1 auto',
    },
    gridCell:{
        display:'flex',
        alignItems: 'center',
        justifyContent: 'left'
    },
    gridCellLeft:{
        justifyContent: 'left'
    }
}));

const PurchaseItem = (props) => {
    const classes = useStyles();
    const {item, updateHandler, removeHandler} = props;
    const {productItem} = item;
    const [count, setCount] = useState(props.item.count);
    const [totalPrice, setTotalPrice] = useState(props.item.count * productItem.price);

    const handleChange = e => {
        if  (!isNaN(parseInt(e.target.value)) && parseInt(e.target.value) > 0) {
            setCount(parseInt(e.target.value));
            setTotalPrice(parseInt(e.target.value) * productItem.price);
            updateHandler(productItem, parseInt(e.target.value));
        }
    }

    const handleRemove = () => {
        removeHandler(productItem);
    }
    return (
        <ListItem className={classes.listItem} divider={true} disableGutters={true}>
            <Grid container>
                <Grid item xs={2} className={classes.gridCell}>
                    <Avatar src={productItem.avatar} />
                </Grid>
                <Grid item xs={6} className={classes.gridCell}>
                    <Typography align="right">{productItem.title}</Typography>
                </Grid>
                <Grid item xs={3} className={classes.gridCell}>
                    <Typography variant="caption" align="right">{`${productItem.price} هزار تومان`}</Typography>
                </Grid>
                <Grid item xs={1} className={classes.gridCell}>
                    <IconButton className={classes.xButton} onClick={handleRemove}>
                        <Delete />
                    </IconButton>
                </Grid>
                <Grid item xs={2} />
                <Grid item xs={1} className={classes.gridCell} classes={{container:classes.gridCellLeft}}>
                    <Typography variant="caption" align="left">تعداد:</Typography>
                </Grid>
                <Grid item xs={2} className={classes.gridCell}>
                    <TextField type="number" size="small" value={count} onChange={handleChange} style={{margin:10, maxWidth: 30}} />
                </Grid>
                <Grid item xs={1} />
                <Grid item xs={2} className={classes.gridCell}>
                    <Typography variant="caption" align="left">هزینه کل:</Typography>
                </Grid>
                <Grid item xs={3} className={classes.gridCell}>
                    <Typography variant="body1" align="right">{`${totalPrice} هزار تومان`}</Typography>
                </Grid>
            </Grid>
        </ListItem>
    );
};

export default PurchaseItem;