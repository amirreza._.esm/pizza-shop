import React, { useState, useEffect } from 'react';
import { Paper, List, Typography, Button, makeStyles, Divider, TextField, Grid, useTheme, FormControlLabel, Radio } from '@material-ui/core';
import PurchaseItem from './PurchaseItem';
import { coupons } from '../../mocks';
import { lightGreen, red } from '@material-ui/core/colors';

const useStyles = makeStyles(theme => ({
    root: {
        minHeight: 450,
        minWidth: 450,
        direction : 'rtl',
        paddingTop: 10,
        paddingBottom: 10,
        paddingLeft: 20,
        paddingRight: 20,
        display:'flex',
        flexDirection: 'column',
        alignItems: 'center'
    },
    title:{
        marginBottom:20
    },
    selectList: {
        overflowY : 'auto',
        maxHeight: 350,
        minWidth: 400,
        flex: 1
    },
    totalPriceContainer :{
        marginBottom:10
    },
    couponBtn: props => ({
        color: props.couponBtnColor
    }),
    errorContainer : {
        flex: 1,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    },
    paymentContainer: {
        marginBottom: 20
    }
}));

const PurchaseList = (props) => {
    const theme = useTheme();
    const [couponBtnColor, setCouponBtnColor] = useState(theme.palette.primary.main);
    const classes = useStyles({couponBtnColor});
    const {selectList, updateHandler, removeHandler, closeDialog} = props;
    const [totalPrice, setTotalPrice] = useState(0) ;
    const [couponCode, setCouponCode] = useState('');
    const [couponDiscount, setCouponDiscount] = useState(0);
    const [paymentType, setPaymentType] = useState("cash");
    

    useEffect(() => {
        setTotalPrice(0);
        selectList.forEach(el => (setTotalPrice(t => t + (el.productItem.price * el.count))));
        setTotalPrice(t => t * (1 - couponDiscount));
    }, [selectList, couponDiscount])
    
    const handleCouponCodeChange = e => {
        setCouponCode(e.target.value);
        setCouponDiscount(0);
        setCouponBtnColor(theme.palette.primary.main);
    };

    const handleCouponCode = () => {
        if(couponCode !== '') {
            const coupon = coupons.find(el => el.code === couponCode);
            if (coupon) {
                setCouponDiscount(coupon.discount);
                setCouponBtnColor(lightGreen[500]);
            } else {
                setCouponBtnColor(red[500]);            
            }
        }
    };

    const handleChange = e => {
        setPaymentType(e.target.value);
    }

    const renderContent = () => {
        return(
            <>
                <List className={classes.selectList}>
                    {selectList.map(item => (
                        <PurchaseItem key={item.productItem.id} item={item} updateHandler={updateHandler} removeHandler={removeHandler}/>
                    ))}
                </List>
                <Grid className={classes.totalPriceContainer} container alignItems="center" spacing={1} >
                    <Grid item xs={3}>
                        <TextField variant="outlined" size="small" value={couponCode} onChange={handleCouponCodeChange} label="کد تخفیف"/>
                    </Grid>
                    <Grid item xs={2}>
                        <Button className={classes.couponBtn} onClick={handleCouponCode}>بررسی</Button>
                    </Grid>
                    <Grid item xs={1}/>
                    <Grid item xs={2}>
                        <Typography variant="caption">قیمت نهایی:</Typography>
                    </Grid>
                    <Grid item xs={4}>
                        <Typography variant="h6" gutterBottom={false}> {`${totalPrice} هزار تومان`}</Typography>
                    </Grid>
                </Grid>
                <Grid container alignItems="center" className={classes.paymentContainer}>
                    <Grid item xs={2}>
                        <Typography> نوع پرداخت:</Typography>
                    </Grid>
                    <Grid item xs={3}>
                        <FormControlLabel checked={paymentType==="cash"} label="نقدی" value="cash" onChange={handleChange} control={ <Radio /> }/>
                    </Grid>
                    <Grid item xs={3}>
                        <FormControlLabel checked={paymentType==="online"} label="بانکی" value="online" onChange={handleChange} control={ <Radio /> }/>
                    </Grid>        
                    <Grid item xs={3}>
                        <FormControlLabel checked={paymentType==="bitcoin"} label="بیت کوین" value="bitcoin" onChange={handleChange} control={ <Radio /> }/>
                    </Grid>
                    <Grid item xs={1}/>
                </Grid>
            </>
        );
    }
    const renderError = () => {
        return(
            <div className={classes.errorContainer}>
                <Typography align="center">
                    سبد خرید شما خالیست...
                </Typography>
            </div>
        );
    }

    return (
        <Paper className={classes.root}>
            <Typography variant="h5" align="center" className={classes.title}>سبد خرید</Typography>
            {
                selectList.length > 0 
                ? renderContent(): renderError() 
            }
            <Grid container spacing={1}>
                <Grid item xs={2}>
                    <Button onClick={closeDialog} size="large">بازگشت</Button>
                </Grid>
                <Grid item xs={2}>
                    <Button disabled={!(selectList.length > 0)} size="large" variant="contained" color="primary">پرداخت</Button>
                </Grid>
            </Grid>
        </Paper>
    );
};

export default PurchaseList;