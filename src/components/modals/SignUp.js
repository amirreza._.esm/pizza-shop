import React, { useState, useEffect } from 'react';
import { Paper, List, Typography, Button, makeStyles, Divider, TextField, Grid, useTheme, FormControlLabel, Radio, InputAdornment, Checkbox } from '@material-ui/core';
import { Lock, AccountCircle, LocalPizza, AlternateEmail, BusinessSharp } from '@material-ui/icons';

const useStyles = makeStyles(theme => ({
    paper: {
        padding: 30,
        paddingBottom: 0,
        display:'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    form: {
        width: 275,
        margin: 25,
        marginTop: 20
    }
}));

const SignUp = (props) => {

    const classes = useStyles();

    return (
        <Paper className={classes.paper}>
            <Typography color="secondary" style={{fontWeight:700}} variant="h4" align="center" >ثبت نام</Typography>
            <LocalPizza color="primary" style={{marginTop: 20, fontSize: '4em'}}/>
            {/* <div style={{height:'300px', display:'flex', flexDirection: 'column', justifyContent:'center'}}> */}
                <div className={classes.form}>
                    <TextField style={{marginTop: 15, marginBottom: 10}}
                        variant="outlined" size="small"
                        fullWidth
                        label="ایمیل"
                        InputProps={{
                            startAdornment: (
                                <InputAdornment position="start">
                                    <AlternateEmail />
                                </InputAdornment>
                            )
                        }}
                    />
                    <TextField style={{marginTop: 15, marginBottom: 10}}
                        variant="outlined" size="small"
                        fullWidth
                        label="نشانی"
                        InputProps={{
                            startAdornment: (
                                <InputAdornment position="start">
                                    <BusinessSharp />
                                </InputAdornment>
                            )
                        }}
                    />
                    <TextField style={{marginTop: 15, marginBottom: 10}}
                    type="password"
                        variant="outlined" size="small"
                        fullWidth
                        label="رمزعبور"
                        InputProps={{
                            startAdornment: (
                                <InputAdornment position="start">
                                    <Lock />
                                </InputAdornment>
                            )
                        }}
                    />
                    <TextField style={{marginTop: 15, marginBottom: 10}}
                        type="password"
                        variant="outlined" size="small"
                        fullWidth
                        label="تکرار رمزعبور"
                        InputProps={{
                            startAdornment: (
                                <InputAdornment position="start">
                                    <Lock />
                                </InputAdornment>
                            )
                        }}
                    />
                    <FormControlLabel label="عضویت در خبرنامه" 
                        control={(<Checkbox value="rss"/>)}/>
                    <Button style={{marginTop: 20}} color="primary" fullWidth variant="contained" size="large">ثبت نام</Button>
                </div>
            {/* </div> */}
        </Paper>
    );
};

export default SignUp;