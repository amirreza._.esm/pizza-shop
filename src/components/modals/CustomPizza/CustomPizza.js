import React, { useState } from 'react';
import { Paper, Stepper, Step, StepLabel, makeStyles, Button, FormControl, FormLabel, RadioGroup, FormControlLabel, Radio, Typography, TextField } from '@material-ui/core';
import { customContent, imgCustom } from '../../../mocks';

const useStyles = makeStyles(theme => ({
    root : {
        direction : 'rtl',
        padding: 15
    },
    controlContainer:{
        display:'flex'
    },
    controlSpacer:{
        flexGrow:1
    }
}));


const CustomPizza = (props) => {
    const classes = useStyles();
    const [activeStep, setActiveStep] = useState(0);
    const [priceList, setPriceList] = useState(customContent.map(e=> e.options[0].price));
    const stepContent = [...customContent, {isResultsPage: true, title: "آماده"}]; 
    const [title, setTitle] = useState('');
    const [description, setDescription] = useState('');
    const {addHandler, closePizza} = props;


    const addToList = p => {
        setPriceList(list => [...list, p]);
    }

    const changeElement = (e, index) => {
        setPriceList(priceList.map((el, i) => {
            if (i === index) return e;
            return el;
        }));
    }

    const handleNext = (step) => {
        if (activeStep < stepContent.length)
            setActiveStep(step => step + 1);
    }

    const handlePrevOrReturn = (step) => {
        if (activeStep > 0)
            setActiveStep(step => step - 1);
        else if (activeStep === 0) 
            closePizza();
    }

    const handleNextOrAdd = (step) => {
        if (activeStep === stepContent.length - 1){
            addHandler({
                id: 500,
                title: title,
                description: description,
                avatar: imgCustom,
                price: priceList.reduce((p,c) => p + c)
            });
            closePizza();
        } else {
            handleNext(step);
        }
    }

    const handleTitleChange = e => {
        setTitle(e.target.value);
    }
    
    const handleDescriptionChange = e => {
        setDescription(e.target.value);
    }

    const renderContent = step => {        
        if (stepContent[step].isResultsPage) {
            return (
                <>
                    <Typography>پیتزای شما آماده است.</Typography>
                    <TextField variant="outlined" onChange={handleTitleChange} label="نام"/>
                    <TextField variant="outlined" onChange={handleDescriptionChange} label="توضیحات"/>
                    <Typography> {`${priceList.reduce((p,c) => p + c)} هزار تومان`}</Typography>
                </>
            );
        }
        return(
            <FormControl component="fieldset">
                <FormLabel component="legend">
                    <RadioGroup defaultValue={stepContent[step].options[0].text}>
                        {stepContent[step].options.map(option => (
                            <FormControlLabel color="default" value={option.text} onChange={()=>{changeElement(option.price, step)}} label={`${option.text}: ${option.price} هزار تومان`} control={<Radio />}/>
                        ))}
                    </RadioGroup>
                </FormLabel>
            </FormControl>
        );                
    }

    return(
        <Paper className={classes.root}>
            <Typography variant="h5" align="center">پیتزای سفارشی</Typography>
            <Stepper activeStep={activeStep}>
                {
                    stepContent.map((c, index) => {
                        return (
                            <Step>
                                <StepLabel>{c.title}</StepLabel>
                            </Step>
                        );
                    })
                }
            </Stepper>
            {renderContent(activeStep)}
            <div className={classes.controlContainer}>
                <Button onClick={()=>handlePrevOrReturn(activeStep)}>{activeStep === 0 ? "بازگشت" : "قبلی"}</Button>
                <div className={classes.controlSpacer}></div>
                <Button 
                    onClick={()=>handleNextOrAdd(activeStep)}
                    color={activeStep === stepContent.length - 1 ? "primary": "default"}>{activeStep === stepContent.length - 1 ? "اضافه به سبد خرید" : "بعدی"}</Button>
            </div>
        </Paper>
    );
}

export default CustomPizza;

