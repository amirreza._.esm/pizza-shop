import React from 'react';
import { Typography, makeStyles, Paper, Grid, Card, CardHeader, Divider, TextField, InputAdornment, InputLabel, Container, Button, List, ListItem, ListItemText, CardContent } from '@material-ui/core';
import bgImg from '../imgRes/background4.jpg'
import { Email, Comment, AccountBox } from '@material-ui/icons';

const useStyles = makeStyles(theme => ({
    paperContainer: {
        marginTop: '35px',
        marginBottom : '35px',
        display: 'flex',
        flexDirection:'column',
        justifyContent: 'center',
        alignItems: 'center',
        direction: 'rtl',
    },
    paper: {
        width: '50%',
        maxWidth: "600px",
        padding: '25px',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    commentPaper: {
        marginTop: 20,
        width: '50%',
        maxWidth: "600px",
        padding: '25px',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    grid: {
        maxWidth: '200px'
    },
    divider: {
        // margin: '0 20px',
        marginTop: 25,
        marginBottom: 15,
        width: '100%',
        height: 1,
        backgroundColor: 'grey'
    },
    commentContainer:{
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    }
}));

export default function About() {
    const classes = useStyles();
    return (
        <div className={classes.paperContainer}>
            <Paper className={classes.paper}>
                <Typography align="center" variant='h2'>درباره ما</Typography>
                <Typography variant='body1'>برای اولین بار مفهوم پیتزا آنلاین با قابلیت ساخت پیتزای دلخواه ( پیتزاتوبساز ) را در کمترین زمان تحویل "چهل دقیقه یا کمتر" درایران پیاده سازی ...</Typography>
                <Grid container className={classes.grid} spacing={3}>
                    <Grid item xs={12}>
                        <Typography align="center" variant='h6'>ارتباط با ما</Typography>
                    </Grid>
                    <Grid item xs={6} style={{textAlign: 'left'}}>
                        <Typography variant='body2'>تلفن:</Typography>
                    </Grid>
                    <Grid item xs={6}>
                        <Typography variant='body2'>09281221</Typography>
                    </Grid>
                    <Grid item xs={6} style={{textAlign: 'left'}}>
                        <Typography variant='body2'>آدرس:</Typography>
                    </Grid>
                    <Grid item xs={6}>
                        <Typography variant='body2'>شسیشسی-dsaشسیشسd-شسیشسیش-sad2</Typography>
                    </Grid>
                </Grid>
                <div className={classes.divider}></div>
            </Paper>
            <Paper className={classes.commentPaper}>
                <Container className={classes.commentContainer}>
                    <Typography variant="h6" align="center">ارسال دیدگاه</Typography>
                    <TextField style={{marginTop: 15, marginBottom: 10}}
                        variant="outlined" size="small"
                        fullWidth
                        label="ایمیل"
                        InputProps={{
                            startAdornment: (
                                <InputAdornment position="start">
                                    <Email />
                                </InputAdornment>
                            )
                        }}
                    />
                    <TextField style={{marginBottom: 10}} 
                        variant="outlined" size="small"
                        fullWidth
                        multiline
                        label="دیدگاه"
                        InputProps={{
                            startAdornment: (
                                <InputAdornment position="start">
                                    <Comment />
                                </InputAdornment>
                            )
                        }}
                    />
                    <Button variant="outlined" color="secondary" fullWidth>ارسال</Button>
                </Container>
                <div className={classes.divider}></div>
                <Container>
                    <Typography style={{marginBottom:10}} variant="h6" align="center">دیدگاه های مشتریان</Typography>
                        <Card style={{marginTop:10}}>
                            <CardContent>
                                <div style={{display:'flex', flexDirection:'row-reverse'}}>
                                    <AccountBox style={{marginRight:10}}/>
                                    <Typography align="left" >abcde@gmail.com</Typography>
                                </div>
                                <div className={classes.divider} style={{marginTop:5}}></div>
                                <Typography align="right" fullWidth>سرویس عالی بلسشیمنسئ سشیدشسی صدشسوی نشسی </Typography>
                            </CardContent>
                        </Card>
                        <Card style={{marginTop:10}}>
                            <CardContent>
                                <div style={{display:'flex', flexDirection:'row-reverse'}}>
                                    <AccountBox style={{marginRight:10}}/>
                                    <Typography align="left" >asdgwqewq@yahoo.com</Typography>
                                </div>
                                <div className={classes.divider} style={{marginTop:5}}></div>
                                <Typography align="right" fullWidth>ضصخث ض صثشئیمک نوشسنیمئمنش ئصض ضصمن ئیضئسشنیک سشیدشسی صدضثضصشسوی نشسی </Typography>
                            </CardContent>
                        </Card>
                        <Card style={{marginTop:10}}>
                            <CardContent>
                                <div style={{display:'flex', flexDirection:'row-reverse'}}>
                                    <AccountBox style={{marginRight:10}}/>
                                    <Typography align="left" >kasdqw@hotmail.com</Typography>
                                </div>
                                <div className={classes.divider} style={{marginTop:5}}></div>
                                <Typography align="right" fullWidth>شنضصث مضنصث شکلم سشیدشضشسی مکضثص کضحصث </Typography>
                            </CardContent>
                        </Card>
                        <Card style={{marginTop:10}}>
                            <CardContent>
                                <div style={{display:'flex', flexDirection:'row-reverse'}}>
                                    <AccountBox style={{marginRight:10}}/>
                                    <Typography align="left" >oqweqope@gmail.com</Typography>
                                </div>
                                <div className={classes.divider} style={{marginTop:5}}></div>
                                <Typography align="right" fullWidth>وشس ی عالی یمئشسنیمنصضئثنمئسش.  سسشمی ضصث شیدشسی صدشسوی نشسی </Typography>
                            </CardContent>
                        </Card>
                </Container>
            </Paper>
        </div>
    );
}
