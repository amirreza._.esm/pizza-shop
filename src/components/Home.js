import React from 'react';
import { Typography, Fab, makeStyles } from '@material-ui/core';
import FoodListContainer from './Food/FoodListContainer';
import { LocalPizzaRounded } from '@material-ui/icons';
import bgImg from '../imgRes/background3.png'

const useStyles = makeStyles(theme => ({
    root: {
        position: 'relative',
        backgroundColor: theme.palette.secondary.light
    },
    fab: {
        position: 'sticky',
        bottom : 16,
        left: 16,
        marginBottom: 16,
        marginRight : 16
    },
    bgContainer: {
        position: 'absolute',
        backgroundImage: `url(${bgImg})`,
        width : '100%',
        top: 0,
        bottom : 0,
        backgroundAttachment: 'fixed',
        backgroundSize:     'cover',
        backgroundRepeat:   'no-repeat',
        backgroundPosition: 'center center',    
    }
}));

export default function Home(props) {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <div className={classes.bgContainer}></div>
            <FoodListContainer {...props}/>
            <Fab onClick={props.openPizzaHandler} className={classes.fab} color="default">
                <LocalPizzaRounded />
            </Fab>
        </div>
    );
}
