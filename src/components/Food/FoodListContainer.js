import React, { useState } from 'react';
import { Tabs, Tab, makeStyles, Typography, Box } from '@material-ui/core';
import MainList from './MainList';
import { americanList, italianList, drinkList, burgerList } from '../../mocks';
import SwipeableViews from 'react-swipeable-views';

const useStyles = makeStyles(theme => ({
    tabs:{
        direction:'rtl',
        position: 'sticky',
        backgroundColor: theme.palette.secondary.light,
        width: 800 ,
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10,
        margin: '0 auto',
        // backgroundImage: 'linear-gradient(#00000000, #00000050, #000000e0, #000000e0, #00000050, #00000000)',
        top: 60,
        zIndex: 1
    },
    tabsIndicator:{
        marginBottom:10,
        height:5,
        borderBottomLeftRadius:5,
        borderBottomRightRadius:5,
    },
    tabsContainer:{
        padding:10
    },
    tabUnselected:{
        // color:theme.palette.primary.main,
    },
    tab:{
        borderRadius:5,
        color:theme.palette.text.primary,
        backgroundColor:theme.palette.primary.light,
        boxShadow:'0 1px 2px 2px #00000030',
        transition:'0.1s',
        '&:hover':{
            boxShadow:'0 2px 3px 3px #00000040',
            '&$tabsIndicator':{
                color:'blue'
            }          
        }  
    },
    tabContent : {
    }
}));

function TabPanel(props) {
    const { children, value, index, ...other } = props;
  
    return (
      <Typography
        component="div"
        role="tabpanel"
        hidden={value !== index}
        id={`full-width-tabpanel-${index}`}
        aria-labelledby={`full-width-tab-${index}`}
        {...other}
      >
        {value === index && <Box p={3}>{children}</Box>}
      </Typography>
    );
  }

export default function FoodListContainer(props) {
const classes = useStyles();
const [tab, setTab] = useState(0);
const {addHandler} = props;

const handleTabChange = (e, nextVal) =>{
    setTab(nextVal);
};

const handleChangeIndex = index => {
    setTab(index);
};

    return(
        <div>
            <Tabs value={tab} onChange={handleTabChange} centered className={classes.tabs}
            classes={{
                indicator:classes.tabsIndicator,
                flexContainer:classes.tabsContainer
            }}>
                <Tab className={tab===0 ? classes.tab : classes.tabUnselected} label="آمریکایی" />
                <Tab className={tab===1 ? classes.tab : classes.tabUnselected} label="ایتالیایی" />
                <Tab className={tab===2 ? classes.tab : classes.tabUnselected} label="برگر" />
                <Tab className={tab===3 ? classes.tab : classes.tabUnselected} label="نوشیدنی و سالاد" />
            </Tabs>
            <SwipeableViews
                className={classes.tabContent}
                axis='x-reverse'
                index={tab}
                onChangeIndex={handleChangeIndex}
            >
                <TabPanel value={0} index={tab}>
                    <MainList addHandler={addHandler} productList={americanList}/>
                </TabPanel>
                <TabPanel value={1} index={tab}>
                    <MainList addHandler={addHandler} productList={italianList}/>
                </TabPanel>
                <TabPanel value={2} index={tab}>
                    <MainList addHandler={addHandler} productList={burgerList}/>
                </TabPanel>
                <TabPanel value={3} index={tab}>
                    <MainList addHandler={addHandler} productList={drinkList}/>
                </TabPanel>
            </SwipeableViews>
        </div>
    );
};


