import React from 'react';
import { Card, makeStyles, Avatar, Button, Typography } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
    container:{
        padding: 10,
        paddingTop: 110
    },
    card:{
        // borderTopLeftRadius:50,
        // borderTopRightRadius:50, 
        // borderBottomLeftRadius:50,
        // borderBottomRightRadius:50,
        borderRadius:20, 
        boxSizing:'border-box',
        width:300,
        height:300,
        padding:20,
        backgroundColor:theme.palette.secondary.dark,
        position:'relative',
        display:'flex',
        flexDirection:'column',
        alignItems:'center',
        direction:'rtl',
        textAlign:'center',
        boxShadow:'0 0 2px 2px #00000030',
        transition:'0.1s',
        '&:hover':{
            boxShadow:'0 1px 3px 3px #00000040',
            transform:'translateY(-1px)'
        }
    }, 
    avatarBg: {
        backgroundColor:theme.palette.secondary.dark,
        borderRadius:100,
        width:200,
        height:200,
        position:'absolute',
        top:'-100px'
    }, 
    avatar: {
        width:150,
        height:150,
        position:'absolute',
        top:'-75px',
        boxShadow:`0 0 2px 2px ${theme.palette.primary.light}`,
    },
    title:{
        marginTop:75,
        color:theme.palette.primary.light,
    },
    description:{
        marginTop:10,
        color:theme.palette.primary.dark
    },
    price:{
        color:theme.palette.primary.light,
        position:'absolute',
        bottom: 75        
    },
    button:{
        position:'absolute',
        bottom: 25
    }
}));

export default function FoodCard(props) {
    const classes = useStyles();
    const {foodItem , addHandler} = props;

    const handleAddItem = () => {
        addHandler(foodItem);
    };
    return(
        <div className={classes.container}>
            <div className={classes.card}>
                <div className={classes.avatarBg}></div>
                <Avatar className={classes.avatar} src={foodItem.avatar} />
                <Typography variant="h6" className={classes.title}>{foodItem.title}</Typography>
                <Typography variant="caption" className={classes.description}>{foodItem.description}</Typography>
                <Typography variant="subtitle1" className={classes.price}>{`${foodItem.price} هزار تومان`}</Typography>
                <Button variant="contained" color="primary" className={classes.button} onClick={handleAddItem}>افزودن به سبد خرید</Button>
            </div>
        </div>
    );
}