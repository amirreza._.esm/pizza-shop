import React from 'react';
import { GridList, Box, makeStyles, GridListTile, useMediaQuery, Typography } from '@material-ui/core';
import FoodCard from './FoodCard';

const useStyles = makeStyles(theme => ({
    gridList:{
        width:'80%',
        maxWidth:'1000px',
    },
    gridListTile:{
        display:'flex',
        justifyContent:'center'
    }
}));

export default function MainList(props) {
    const classes = useStyles();
    const {productList, addHandler} = props;

    const mediaSm = useMediaQuery(theme => theme.breakpoints.down("xs"));
    const mediaDm = useMediaQuery(theme => theme.breakpoints.down("sm"));

    let cols = 3;
    if(mediaSm) cols = 1;
    else if (mediaDm) cols = 2; 

    return(
        <GridList style={{margin:'20px auto'}} cellHeight="auto" className={classes.gridList} cols={cols} >
            {
                productList.map((foodItem)=> (
                    <GridListTile classes={{tile: classes.gridListTile}}>
                        <FoodCard foodItem={foodItem} addHandler={addHandler}/>
                    </GridListTile>
                ))
            }
        </GridList>
    );
};
